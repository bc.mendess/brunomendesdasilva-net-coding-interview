using Moq;
using SecureFlight.Core.DTOs;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Core.Services;
using Xunit;

namespace SecureFlight.Test.Services
{
    public class FlightServiceTests
    {
        private readonly Mock<IRepository<Flight>> _repositoryMock;
        private readonly Mock<IRepository<Passenger>> _repositoryPassengerMock;
        private readonly FlightService _service;

        public FlightServiceTests()
        {
            _repositoryMock = new Mock<IRepository<Flight>>();
            _repositoryPassengerMock = new Mock<IRepository<Passenger>>();
            _service = new FlightService(_repositoryMock.Object, _repositoryPassengerMock.Object);
        }

        [Fact]
        public void AddPassengerToFlight_WhenValidDTO_ShouldSucceeded()
        {
            // Given
        
            // When

        
            // Then
        }

        [Fact]
        public void AddPassengerToFlight_WhenInvalidIdFlight_ShouldReturnError()
        {
            // Given
            var addPassengerToFlight = new AddPassengerToFlightDataTransferObject{
                IdFlight = 123,
                IdPassenger = "321"
            };

            //_repositoryMock.
        
            // When
        
            // Then
        }

        [Fact]
        public void AddPassengerToFlight_WhenInvalidIdPassenger_ShouldReturnError()
        {
            // Given
        
            // When
        
            // Then
        }
    }
}