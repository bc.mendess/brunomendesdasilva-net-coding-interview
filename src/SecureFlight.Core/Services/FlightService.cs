using System.Threading.Tasks;
using SecureFlight.Core.DTOs;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Core.Services
{
    public class FlightService : BaseService<Flight>, IFlightService
    {
        private readonly IRepository<Passenger> _passengerRepository;

        public FlightService(IRepository<Flight> repository, IRepository<Passenger> passengerRepository) : base(repository)
        {
            _passengerRepository = passengerRepository;
        }

        public Task<OperationResult> AddPassengerToFlight(AddPassengerToFlightDataTransferObject addPassengerToFlight)
        {
            throw new System.NotImplementedException();
        }
    }
}