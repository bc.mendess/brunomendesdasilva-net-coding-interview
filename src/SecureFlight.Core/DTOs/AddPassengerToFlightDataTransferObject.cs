namespace SecureFlight.Core.DTOs
{
    public class AddPassengerToFlightDataTransferObject
    {
        public long IdFlight { get; set; }
        public string IdPassenger { get; set; }
    }
}