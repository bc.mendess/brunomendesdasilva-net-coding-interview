using System.Threading.Tasks;
using SecureFlight.Core.DTOs;
using SecureFlight.Core.Entities;

namespace SecureFlight.Core.Interfaces
{
    public interface IFlightService : IService<Flight>
    {
        Task<OperationResult> AddPassengerToFlight(AddPassengerToFlightDataTransferObject addPassengerToFlight);
    }
}